<?php

return [
    'application' => [
        'name' => 'Url Shortener',
    ],
    'db'          => [
        'host'     => '127.0.0.1',
        'dbname'   => 'urlshortener',
        'username' => 'root',
        'password' => '',
    ],
    'router'      => [
        ''                             => ['Default', 'index'],
        '/^(?<url>[a-zA-Z0-9]{1,6})$/' => ['Url', 'redirect', ['url' => ':url']],
    ],
];