$(document).ready(function(){
    onSubmitEvent($('form'));
});

function onSubmitEvent($form) {
    $form.on('submit', function () {
        doSubmit($form);
        return false;
    });
}

function doSubmit($form) {
    $('.error').html('');
    $.ajax({
        url: $form.attr('action'),
        method: 'post',
        data: $form.serialize(),
        dataType: "json",
        success: function(response) {
            if (response.result) {
                console.log(response.shorturl);
                $('#result').html(response.shorturl);
            } else {
                $('.error').html(response.message);
            }
        }
    });
}
