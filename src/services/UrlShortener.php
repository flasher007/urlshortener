<?php

namespace App\Service;


class UrlShortener
{

    CONST SHORT_URL_LENGHT = 5;

    /**
     * @var string
     */
    protected static $index = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    /**
     * generate short code from url
     *
     * @param $longUrl
     *
     * @return string
     */
    public static function encode($longUrl)
    {
        return substr(str_shuffle(self::$index), 0, self::SHORT_URL_LENGHT);
    }
}