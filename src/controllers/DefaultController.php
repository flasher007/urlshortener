<?php
namespace App\Controllers;

use App\Service\UrlShortener;
use App\Model\Url;
use App\Form\UrlForm;
use System\Kernel;

class DefaultController extends \System\Controller
{

    /**
     * @return array|string
     * @throws \Exception
     */
    public function indexAction()
    {
        $form = new UrlForm();
        $result = false;
        $shortUrl = '';

        if ($this->request->isPost()) {
            $url = $this->request->getPostVar('url');
            $form->setValue('url', $url);

            if ($form->validate()) {
                $urlModel = new Url();
                $existUrl = $urlModel->findOneBy('url', $url);

                if (false === $existUrl) {
                    $shortUrl           = UrlShortener::encode($url);
                    $urlModel->url      = $url;
                    $urlModel->shorturl = $shortUrl;
                    $urlModel->save();
                } else {
                    $shortUrl = $existUrl->shorturl;
                }

                $shortUrl = Kernel::getInstance()->router->createUrl('Url', 'redirect', ['url' => $shortUrl]);
                $form->setValue('shorturl', $shortUrl);
                $result = true;
            }
        }
        if ($this->request->isAjaxRequest()) {
            return [
                'result'   => $result,
                'message'  => implode('<br>', $form->getMessages()),
                'shorturl' => $shortUrl
            ];
        } else {
            return $this->view->render('default/index.phtml', ['form' => $form]);
        }
    }
}