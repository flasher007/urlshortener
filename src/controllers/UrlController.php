<?php

namespace App\Controllers;

use App\Model\Url;

class UrlController extends \System\Controller
{

    /**
     * @return string
     * @throws \Exception
     */
    public function redirectAction()
    {
        $shortUrl = $this->request->getQueryVar('url');

        $model    = new Url();
        $urlModel = $model->findOneBy('shorturl', $shortUrl);

        if (false !== $urlModel && !empty($urlModel->url)) {
            $this->response->redirect($urlModel->url, \System\Response::HTTP_MOVED_PERMANENTLY);
        } else {
            $this->response->sendNotFound();

            return $this->view->render('error/404.phtml');
        }
    }

}