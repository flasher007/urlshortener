<?php

namespace App\Form;

use \System\Form\FormBuilder;

class UrlForm extends FormBuilder
{

    /**
     * @inheritdoc
     */
    protected function init()
    {
        $this->setField('url')
            ->setRule(
                'Enter a valid url',
                function ($value) {
                    $value = trim($value);

                    return filter_var($value, FILTER_VALIDATE_URL);
                }
            );
        $this->setField('shorturl');

        $this->setField('go', 'submit')->setValue('Do!');
    }
}