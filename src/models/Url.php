<?php

namespace App\Model;

use \System\Model;

class Url extends Model
{
    protected $table       = 'url';
    protected $primaryKey  = 'id';
    protected $columnNames = ['id', 'url', 'shorturl'];

    /**
     * Check url in database before save
     * @return bool
     * @throws \Exception
     */
    protected function preSave()
    {
        if (!empty($this->url) && empty($this->{$this->primaryKey})) {
            $findUrl = $this->findOneBy('url', $this->url);

            if ($findUrl) {
                $this->url      = $findUrl->url;
                $this->shorturl = $findUrl->shorturl;
                $this->id       = $findUrl->id;
            }
        }

        return true;
    }

}