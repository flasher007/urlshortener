<?php

namespace System;


class Response
{
    const HTTP_CONTINUE                                                  = 100;
    const HTTP_SWITCHING_PROTOCOLS                                       = 101;
    const HTTP_PROCESSING                                                = 102;
    const HTTP_OK                                                        = 200;
    const HTTP_CREATED                                                   = 201;
    const HTTP_ACCEPTED                                                  = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION                             = 203;
    const HTTP_NO_CONTENT                                                = 204;
    const HTTP_RESET_CONTENT                                             = 205;
    const HTTP_PARTIAL_CONTENT                                           = 206;
    const HTTP_MULTI_STATUS                                              = 207;
    const HTTP_ALREADY_REPORTED                                          = 208;
    const HTTP_IM_USED                                                   = 226;
    const HTTP_MULTIPLE_CHOICES                                          = 300;
    const HTTP_MOVED_PERMANENTLY                                         = 301;
    const HTTP_FOUND                                                     = 302;
    const HTTP_SEE_OTHER                                                 = 303;
    const HTTP_NOT_MODIFIED                                              = 304;
    const HTTP_USE_PROXY                                                 = 305;
    const HTTP_RESERVED                                                  = 306;
    const HTTP_TEMPORARY_REDIRECT                                        = 307;
    const HTTP_PERMANENTLY_REDIRECT                                      = 308;
    const HTTP_BAD_REQUEST                                               = 400;
    const HTTP_UNAUTHORIZED                                              = 401;
    const HTTP_PAYMENT_REQUIRED                                          = 402;
    const HTTP_FORBIDDEN                                                 = 403;
    const HTTP_NOT_FOUND                                                 = 404;
    const HTTP_METHOD_NOT_ALLOWED                                        = 405;
    const HTTP_NOT_ACCEPTABLE                                            = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED                             = 407;
    const HTTP_REQUEST_TIMEOUT                                           = 408;
    const HTTP_CONFLICT                                                  = 409;
    const HTTP_GONE                                                      = 410;
    const HTTP_LENGTH_REQUIRED                                           = 411;
    const HTTP_PRECONDITION_FAILED                                       = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE                                  = 413;
    const HTTP_REQUEST_URI_TOO_LONG                                      = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE                                    = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE                           = 416;
    const HTTP_EXPECTATION_FAILED                                        = 417;
    const HTTP_I_AM_A_TEAPOT                                             = 418;
    const HTTP_UNPROCESSABLE_ENTITY                                      = 422;
    const HTTP_LOCKED                                                    = 423;
    const HTTP_FAILED_DEPENDENCY                                         = 424;
    const HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;
    const HTTP_UPGRADE_REQUIRED                                          = 426;
    const HTTP_PRECONDITION_REQUIRED                                     = 428;
    const HTTP_TOO_MANY_REQUESTS                                         = 429;
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE                           = 431;
    const HTTP_UNAVAILABLE_FOR_LEGAL_REASONS                             = 451;
    const HTTP_INTERNAL_SERVER_ERROR                                     = 500;
    const HTTP_NOT_IMPLEMENTED                                           = 501;
    const HTTP_BAD_GATEWAY                                               = 502;
    const HTTP_SERVICE_UNAVAILABLE                                       = 503;
    const HTTP_GATEWAY_TIMEOUT                                           = 504;
    const HTTP_VERSION_NOT_SUPPORTED                                     = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL                      = 506;
    const HTTP_INSUFFICIENT_STORAGE                                      = 507;
    const HTTP_LOOP_DETECTED                                             = 508;
    const HTTP_NOT_EXTENDED                                              = 510;
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED                           = 511;

    const VERSION = '1.0';

    /**
     * @var array
     */
    public $headers;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var string
     */
    protected $statusText;

    /**
     * @var bool
     */
    protected $buffering = false;

    /**
     * Status codes translation table.
     * @var array
     */
    public $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        226 => 'IM Used',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Reserved for WebDAV advanced collections expired proposal',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
    ];

    public function __construct($content = '', $status = self::HTTP_OK, $headers = [])
    {
        $this->setHeaders($headers);
        $this->setContent($content);
        $this->setStatusCode($status);
        $this->setVersion(self::VERSION);
    }


    public function sendNotFound()
    {
        $this->setStatusCode(self::HTTP_NOT_FOUND);
        $this->setHeader('Status', $this->statusTexts[self::HTTP_NOT_FOUND]);
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the response status code.
     *
     * @param int $statusCode
     *
     * @return Response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = (int)$statusCode;

        if ($this->isInvalid()) {
            throw new \InvalidArgumentException(sprintf('The HTTP status code "%s" is not valid.', $this->statusCode));
        }

        $this->statusText = isset($this->statusTexts[$this->statusCode])
            ? $this->statusTexts[$this->statusCode]
            : 'unknown status';

        return $this;
    }

    /**
     * @return string
     */
    public function getStatusText()
    {
        return $this->statusText;
    }

    /**
     * @param string $statusText
     */
    public function setStatusText($statusText)
    {
        $this->statusText = $statusText;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * adds headers
     *
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    /**
     * set header by name
     *
     * @param $key
     * @param $value
     */
    public function setHeader($key, $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * Returns a header by name.
     *
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getHeader($key, $default = null)
    {
        return array_key_exists($key, $this->headers) ? $this->headers[$key] : $default;
    }

    /**
     * Is response invalid?
     *
     * @return bool
     */
    public function isInvalid()
    {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }

    /**
     * Is response successful?
     *
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->statusCode >= 200 && $this->statusCode < 300;
    }

    /**
     * Is the response a redirect?
     *
     * @return bool
     */
    public function isRedirection()
    {
        return $this->statusCode >= 300 && $this->statusCode < 400;
    }

    /**
     * Is there a client error?
     *
     * @return bool
     */
    public function isClientError()
    {
        return $this->statusCode >= 400 && $this->statusCode < 500;
    }

    /**
     * Was there a server side error?
     *
     * @return bool
     */
    public function isServerError()
    {
        return $this->statusCode >= 500 && $this->statusCode < 600;
    }

    /**
     * Is the response OK?
     *
     * @return bool
     */
    public function isOk()
    {
        return self::HTTP_OK === $this->statusCode;
    }

    /**
     * Is the response a not found error?
     *
     * @return bool
     */
    public function isNotFound()
    {
        return self::HTTP_NOT_FOUND === $this->statusCode;
    }

    public function enableBuffering()
    {
        if (!$this->buffering) {
            ob_start();
            $this->buffering = true;
        }
    }

    public function clearBuffer()
    {
        if ($this->buffering) {
            echo ob_get_clean();
            $this->buffering = false;
        }
    }

    /**
     * Sends HTTP headers.
     *
     * @return Response
     */
    public function sendHeaders()
    {
        if (headers_sent()) {
            return $this;
        }
        // status
        header(sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText), true, $this->statusCode);

        // headers
        if (count($this->headers) > 0) {
            foreach ($this->headers as $name => $value) {
                header($name . ': ' . $value, false, $this->statusCode);
            }
        }

        return $this;
    }

    /**
     * Sends content for the current web response.
     * @return $this
     */
    public function sendContent()
    {
        $this->clearBuffer();
    }

    /**
     *
     */
    public function jsonResponse()
    {
        $this->setStatusCode(self::HTTP_OK);
        $this->setHeader('Content-Type', 'application/json;charset=utf-8');
    }

    /**
     * @param $url
     * @param $statusCode
     */
    public function redirect($url, $statusCode)
    {
        $this->setStatusCode($statusCode);
        $this->setHeader('Location', $url);
        $this->sendHeaders();
        exit();
    }
}