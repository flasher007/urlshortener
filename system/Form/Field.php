<?php

namespace System\Form;


class Field
{

    /**
     * field type
     * @var string
     */
    protected $type = '';

    /**
     * html attributes as key-value
     * @var array
     */
    protected $attributes = [];

    /**
     * value for the field
     * @var mixed
     */
    protected $value;

    /**
     * the name for the input
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $rule = [];

    /**
     * @var string
     */
    protected $message = '';

    /**
     * Field constructor.
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Field $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return Field $this
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Field $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns this field as an array for use in a view.
     * @return Field
     */
    public function get()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Field $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getRule()
    {
        return $this->rule;
    }


    /**
     * Sets a filter rule on a field.
     * @param $message
     * @param \Closure $closure
     */
    public function setRule($message, \Closure $closure)
    {
        $this->rule = [$message, $closure];
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Filter and validate the value
     * @return bool
     */
    public function validate()
    {
        $this->message = '';

        if (!empty($this->rule)) {
            //get message and closure
            list($message, $closure) = $this->rule;

            $isValid = $closure($this->value);

            if (!$isValid) {
                $this->message = $message;
            }
        }
        return $this->message ? false : true;
    }


}