<?php

namespace System\Form;

abstract class FormBuilder
{
    const DEFAULT_FIELD_TYPE = 'text';

    /**
     * The class to use for creating fields.
     * @var string
     */
    protected $fieldClass = 'System\Form\Field';

    /**
     * form action
     * @var string
     */
    protected $action = '';

    /**
     * inputs in the form
     * @var array
     */
    protected $inputs = [];

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * FormBuilder constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * @return mixed
     */
    abstract protected function init();

    /**
     * Sets a new Field input
     * @param string $name
     * @param string $type
     * @return Field
     */
    public function setField($name, $type = null)
    {
        $this->inputs[$name] = $this->newField($name, $type);
        return $this->inputs[$name];
    }

    /**
     * create a new Field object
     * @param string $name
     * @param string|null $type
     * @return Field
     */
    protected function newField($name, $type = null)
    {
        if (!$type) {
            $type = self::DEFAULT_FIELD_TYPE;
        }
        $class = $this->fieldClass;
        /**
         * @var Field $field
         */
        $field = new $class($type);
        $field->setName($name);

        return $field;
    }

    /**
     * Returns an input for a view.
     * @param string $name
     * @return Field $this
     * @throws \Exception
     */
    public function getField($name = null) {
        if (!$name) {
            return $this;
        }

        if (! isset($this->inputs[$name])) {
            throw new \Exception('Field with name = "' . $name . '" not exist');
        }

        /**
         * @var Field $input
         */
        $input = $this->inputs[$name];
        return $input->get();
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return FormBuilder $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function setValue($name, $value)
    {
        if (! isset($this->inputs[$name])) {
            throw new \Exception('Field with name = "' . $name . '" not exist');
        }
        $input = $this->inputs[$name];
        $input->setValue($value);

        return $this;
    }

    public function validate()
    {
        $this->messages = [];

        foreach ($this->inputs as $fieldName => $input) {
            /**
             * @var Field $input
             */
            if (!$input->validate()) {
                $this->messages[$fieldName] = $input->getMessage();
            }
        }
        return $this->messages ? false : true;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}