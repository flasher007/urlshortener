<?php

namespace System;

use \System\Response;
use \System\Request;
use \System\View;

abstract class Controller
{

    /**
     * @var Request
     */
    protected $request = null;

    /**
     * @var Response
     */
    protected $response = null;

    /**
     * @var Router
     */
    protected $router = null;

    protected $view = null;

    /**
     * base controller constructor
     *
     * @param Request  $request
     * @param Response $response
     * @param View     $view
     */
    public function __construct(Request $request = null, Response $response = null, View $view = null)
    {
        $this->request  = $request !== null ? $request : new Request();
        $this->response = $response !== null ? $response : new Response();
        $this->view     = $view !== null ? $view : new View();

        $this->init();
    }

    /**
     * initialize
     */
    protected function init()
    {
    }
}