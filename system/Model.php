<?php

namespace System;

use \System\DB\PDOConnection;

abstract class Model
{
    /**
     * @var PDOConnection
     */
    protected $db = null;

    /**
     * @var string
     */
    protected $table = '';

    /**
     * @var string
     */
    protected $primaryKey = '';

    /**
     * @var array
     */
    protected $columnNames = [];

    /**
     * @return PDOConnection
     */
    public function getDbConnection()
    {
        if (is_null($this->db)) {
            $this->db = PDOConnection::getConnection();
        }

        return $this->db;
    }

    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @return array
     */
    public function getColumnNames()
    {
        return $this->columnNames;
    }

    /**
     * Returns one instance of current class found by primary key $pk
     *
     * @param mixed $pk
     *
     * @return Model|false
     */
    public function findOneByPk($pk)
    {
        $columns          = $this->getColumnNames();
        $columnsCommaList = implode(',', $columns);
        $tableName        = $this->getTable();
        $primaryKeyColumn = $this->getPrimaryKey();
        $sql              = "SELECT {$columnsCommaList} FROM {$tableName} WHERE {$primaryKeyColumn} = ?";
        $row              = $this->getDbConnection()->getRow($sql, [$pk]);
        if (is_array($row)) {
            $model = new static;
            $model->populate($row);

            return $model;
        }

        return false;
    }

    /**
     * @param $field
     * @param $value
     *
     * @return bool|Model
     * @throws \Exception
     */
    public function findOneBy($field, $value)
    {
        $columns = $this->getColumnNames();

        if (!in_array($field, $columns)) {
            throw new \Exception('Field "' . $field . '" not exist');
        }

        $columnsCommaList = implode(',', $columns);
        $tableName        = $this->getTable();
        $sql              = "SELECT {$columnsCommaList} FROM {$tableName} WHERE {$field} = ?";
        $row              = $this->getDbConnection()->getRow($sql, [$value]);
        if (is_array($row)) {
            $model = new static;
            $model->populate($row);

            return $model;
        }

        return false;
    }

    /**
     * Set object propertie7s with values from $values
     *
     * @param array $values
     */
    public function populate($values)
    {
        $columns = $this->getColumnNames();
        foreach ($columns as $column) {
            $this->{$column} = $values[$column];
        }
    }

    abstract protected function preSave();

    /**
     * save data to table
     * @return bool
     */
    public function save()
    {
        if (!$this->preSave()) {
            return false;
        }

        $columns            = $this->getColumnNames();
        $columnsCommaList   = implode(',', $columns);
        $tableName          = $this->getTable();
        $primaryKeyColumn   = $this->getPrimaryKey();
        $valuesPlaceholders = implode(',', array_fill(0, count($columns), '?')); // -> ?, ?, ?
        $values             = [];

        foreach ($columns as $column) {
            $values[] = $this->{$column};
        }

        $this->getDbConnection()->execute("LOCK TABLES {$tableName} WRITE"); // "transaction lock" for MyISAM tables

        if (isset($this->{$primaryKeyColumn}) && !empty($this->{$primaryKeyColumn})) {
            $sql = "UPDATE TABLE {$tableName} SET " . $this->setUpdate() . " WHERE {$primaryKeyColumn} = ?";
            $this->getDbConnection()->execute($sql, $values);
        } else {
            $sql                       = "INSERT INTO {$tableName} ({$columnsCommaList}) VALUES ({$valuesPlaceholders})";
            $newPk                     = $this->getDbConnection()->execute($sql, $values);
            $this->{$primaryKeyColumn} = $newPk;
        }
        $this->getDbConnection()->execute("UNLOCK TABLES");

        return true;
    }

    /**
     *
     */
    private function setUpdate()
    {
        $primaryKeyColumn = $this->getPrimaryKey();
        $result           = [];

        foreach ($this->getColumnNames() as $columnName) {
            if ($columnName != $primaryKeyColumn) {
                $result[] = "`" . str_replace("`", "``", $columnName) . "`" . "=?";
            }
        }

        return implode(', ', $result);
    }
}