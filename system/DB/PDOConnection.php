<?php

namespace System\DB;

use PDO;
use System\Kernel;

class PDOConnection
{

    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @var string
     */
    protected $host = '127.0.0.1';

    /**
     * @var string
     */
    protected $dbname = '';

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var array
     */
    protected $driverOptions = [];

    /**
     * @var PDOConnection
     */
    private static $db = null;

    /**
     * PDOConnection constructor.
     */
    private function __construct($config = [])
    {
        if (empty($config)) {
            $config = Kernel::getInstance()->getConfig();
        }

        if (isset($config['db'])) {
            foreach ($config['db'] as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                }
            }
        }
        if (null === $this->connection) {
            $dsn = 'mysql:dbname=' . $this->dbname . ';host=' . $this->host;
            $this->connection = new PDO($dsn, $this->username, $this->password, $this->driverOptions);
        }
    }

    /**
     * @return PDO
     */
    public static function getConnection($config = [])
    {
        if(null === self::$db){
            self::$db = new PDOConnection($config);
        }
        return self::$db;
    }

    /**
     * get one row
     * @param $query
     * @param array $parameters
     * @param $mode
     * @return mixed
     */
    public function getRow($query, $parameters = array(), $mode = PDO::FETCH_ASSOC)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute($parameters);
        return $statement->fetch($mode);
    }

    /**
     * @param $query
     * @param array $parameters
     * @return mixed
     */
    public function execute($query, $parameters = array())
    {
        $statement = $this->connection->prepare($query);
        $result = $statement->execute($parameters);
        if (preg_match('/^\s*INSERT\s/i', $query)) {
            return $this->connection->lastInsertId();
        } else {
            return $result;
        }
    }
}