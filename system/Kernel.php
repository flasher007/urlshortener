<?php

namespace System;

class Kernel
{
    use Helper\StringTrait;

    /**
     * @var array
     */
    public $config = [];

    /**
     * @var \System\Response
     */
    public $response = null;

    /**
     * @var \System\Request
     */
    public $request = null;

    /**
     * @var \System\Router
     */
    public $router = null;

    /**
     * current controller
     * @var mixed
     */
    private $controller = null;

    /**
     * current action method
     * @var string
     */
    private $action = '';

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * Kernel constructor.
     *
     * @param array $config
     */
    private function __construct($config = [])
    {
        $this->setConfig($config);
        $this->request  = new Request();
        $this->response = new Response();
        $this->router   = new Router($config);
    }

    /**
     * @param array $config
     *
     * @return Kernel|null
     */
    public static function getInstance($config = [])
    {
        if (!self::$instance) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     *
     */
    public function run()
    {
        $this->response->enableBuffering();

        $actionContent = '';

        $parsedUrl      = $this->router->parseRequestUrl($this->request->getUrl());
        $controllerName = $parsedUrl->getController();

        if (!$this->isControllerValid($controllerName)) {
            $this->response->sendNotFound();
        }

        if (!empty($controllerName)) {

            if (!empty($parsedUrl->getAction())) {
                $actionContent = $this->triggerController($controllerName, $parsedUrl->getAction(), $parsedUrl->getParameters());
            } else {
                $this->action = "index";

                if (!method_exists($controllerName, $this->action)) {
                    $this->response->sendNotFound();
                }

                $actionContent = $this->triggerController($controllerName, $this->action, $parsedUrl->getParameters());
            }
        } else {
            $actionContent = $this->triggerController('DefaultController');
        }

        $layoutView = new View();
        $layoutView->setContent($actionContent);

        if ($this->request->isAjaxRequest()) {
            $layoutView->setLayout('ajax.layout.phtml');
            $layoutView->render($layoutView->getLayout());
            $this->response->jsonResponse();
        }

        if ($this->response->isNotFound()) {
            $this->response->sendHeaders();
        }

        $layoutView->render($layoutView->getLayout());

        $this->response->sendContent();
    }

    /**
     * instantiate controller object and trigger it's action method
     *
     * @param  string $controller
     * @param  string $method
     * @param  array  $parameters
     *
     * @return mixed
     */
    private function triggerController($controller, $method = "index", $parameters = [])
    {
        $controller       = '\\App\\Controllers\\' . $controller;
        $this->controller = new $controller($this->request, $this->response);

        if (!empty($parameters)) {
            return call_user_func_array([$this->controller, $method], $parameters);
        } else {
            return $this->controller->{$method}();
        }
    }

    /**
     * detect if controller is valid
     *
     * @param $controllerClassName
     *
     * @return bool
     */
    private function isControllerValid($controllerClassName)
    {
        if (!empty($controllerClassName)) {
            if (!preg_match('/\A[a-z]+\z/i', $controllerClassName) ||
                !file_exists(APP_PATH . 'controllers/' . $controllerClassName . '.php')
            ) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

    /**
     * detect if action method is valid
     *
     * @param $controller
     * @param $method
     *
     * @return bool
     */
    private function isMethodValid($controller, $method)
    {
        if (!empty($method)) {
            if (!preg_match('/\A[a-z]+\z/i', $method) ||
                !method_exists($controller, $method)
            ) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

}