<?php
namespace System\Helper;

trait StringTrait
{
    /**
     * Convert string from AaaBbbCcc to aaa_bbb_ccc
     * @param $string
     * @param string $splitter
     * @return string
     */
    public function uncamelize($string, $splitter = "_")
    {
        $string = preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0', preg_replace('/(?!^)[[:upper:]]+/', $splitter . '$0', $string));
        return strtolower($string);
    }

    /**
     * Convert string from aaa_bbb_ccc to AaaBbbCcc
     * @param $string
     * @param bool $capitalizeFirstCharacter
     * @return mixed
     */
    public function camelize($string, $capitalizeFirstCharacter = false)
    {
        $string = str_replace(' ', '', ucwords(str_replace(array('-', '_'), ' ', $string)));
        if (!$capitalizeFirstCharacter) {
            $string[0] = strtolower($string[0]);
        }
        return $string;
    }
}