<?php
namespace System;

/**
 * Request class provides access to HTTP request context
 */
class Request
{

    /**
     * @var array
     */
    protected $priority = ['POST', 'GET', 'COOKIE'];

    /**
     * @var array
     */
    protected $post;

    /**
     * @var array
     */
    protected $get;

    /**
     * @var array
     */
    protected $cookie;

    /**
     * @var array
     */
    protected $request;


    /**
     * Request constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        if (isset($config['priority'])) {
            $this->priority = $config['priority'];
        }

        $this->priority = array_map('strtoupper', $this->priority);
        $this->normalizeRequest();
    }

    /**
     *
     */
    protected function normalizeRequest()
    {
        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
            if (isset($_GET)) {
                $_GET = $this->stripSlashes($_GET);
            }
            if (isset($_POST)) {
                $_POST = $this->stripSlashes($_POST);
            }
            if (isset($_REQUEST)) {
                $_REQUEST = $this->stripSlashes($_REQUEST);
            }
            if (isset($_COOKIE)) {
                $_COOKIE = $this->stripSlashes($_COOKIE);
            }
        }
        if (isset($_GET)) {
            $this->get = $_GET;
        }
        if (isset($_POST)) {
            $this->post = $_POST;
        }
        if (isset($_REQUEST)) {
            $this->request = $_REQUEST;
        }
        if (isset($_COOKIE)) {
            $this->cookie = $_COOKIE;
        }
    }

    /**
     * Check for Magic Quotes and remove them
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public function stripSlashes(&$data)
    {
        return is_array($data) ? array_map([$this, 'stripSlashes'], $data) : stripslashes($data);
    }

    /**
     * Returns value of variables being received by HTTP request in order of $this->priority
     *
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed
     */
    public function getVar($name, $default = null)
    {
        foreach ($this->priority as $method) {
            if ('GET' == $method || 'POST' == $method || 'REQUEST' == $method || 'COOKIE' == $method) {
                $method = "_{$method}";
                $var    = $this->{$method};
                if (isset($var[$name])) {
                    return $var[$name];
                }
            }
        }

        return $default;
    }

    /**
     * Returns the named GET parameter value
     * If the GET parameter does not exist, the second parameter to this method will be returned
     *
     * @param string     $name    the GET parameter name
     * @param mixed|null $default the default parameter value if the GET parameter does not exist
     *
     * @return mixed|null the GET parameter value
     */
    public function getQueryVar($name, $default = null)
    {
        if (isset($this->get[$name])) {
            return $this->get[$name];
        }

        return $default;
    }

    /**
     * Set the named value as GET parameter
     *
     * @param string $name  the GET parameter name
     * @param mixed  $value the GET parameter value
     */
    public function addQueryVar($name, $value)
    {
        $this->get[$name] = $value;
    }

    /**
     * Returns the named POST parameter value
     * If the POST parameter does not exist, the second parameter to this method will be returned
     *
     * @param string     $name
     * @param mixed|null $default
     *
     * @return mixed|null
     */
    public function getPostVar($name, $default = null)
    {
        if (isset($this->post[$name])) {
            return $this->post[$name];
        }

        return $default;
    }

    /**
     * Returns the named cookie parameter value
     *
     * @param string $name the cookie name
     *
     * @return mixed|null
     */
    public function getCookie($name)
    {
        if (isset($this->cookie[$name])) {
            return $this->cookie[$name];
        }

        return null;
    }

    /**
     * Returns the request type, such as GET, POST, HEAD, PUT, DELETE
     * @return string
     */
    public function getRequestMethod()
    {
        return strtoupper(isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET');
    }

    /**
     * Returns part of the request URL that is after the question mark
     * @return string
     */
    public function getQueryString()
    {
        return isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
    }

    /**
     * Returns the currently requested URL
     * @return string
     */
    public function getUrl()
    {
        return $this->getRequestUri();
    }

    /**
     * Returns the request URI portion for the currently requested URL
     * @return string
     */
    public function getRequestUri()
    {
        static $requestUri;
        if (!isset($requestUri)) {
            if (isset($_SERVER['REQUEST_URI'])) {
                $requestUri = $_SERVER['REQUEST_URI'];
                if (!empty($_SERVER['HTTP_HOST'])) {
                    if (strpos($requestUri, $_SERVER['HTTP_HOST']) !== false) {
                        $requestUri = preg_replace('/^\w+:\/\/[^\/]+/', '', $requestUri);  // remove host name "????://????/"
                    }
                } else {
                    $requestUri = preg_replace('/^(http|https):\/\/[^\/]+/i', '', $requestUri); // remove host name "????://????/"
                }
            }
        }

        return $requestUri;
    }

    /**
     * Return true if request was sent by HTTP method 'POST'
     * @return bool true if request sent by HTTP method 'POST'
     */
    public function isPost()
    {
        return 'POST' == $this->getRequestMethod();
    }

    /**
     * Return true if request was sent by javascript
     * @return bool true if request sent by javascript
     */
    public function isAjaxRequest()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
        $host     = $_SERVER['SERVER_NAME'];
        $port     =
            (
                ('http' == $protocol && 80 == $_SERVER['SERVER_PORT'])
                ||
                ('https' == $protocol && 443 == $_SERVER['SERVER_PORT'])
            ) ? '' : (':' . $_SERVER['SERVER_PORT']);

        return "{$protocol}://{$host}{$port}";
    }
}