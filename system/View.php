<?php

namespace System;

class View
{
    use Helper\StringTrait;
    /**
     * controller
     * @var object
     */
    public $controller = null;

    /**
     * @var string
     */
    protected $content = null;

    /**
     * @var string
     */
    protected $layout = 'layout.phtml';

    /**
     * @var \System\Kernel
     */
    protected $kernel = null;

    /**
     * View constructor.
     */
    public function __construct()
    {
        $this->kernel = Kernel::getInstance();
    }

    /**
     * @param string $viewFile
     * @param array  $data
     *
     * @return string
     */
    public function render($viewFile = '', $data = [])
    {
        if (!empty($data)) {
            extract($data);
        }

        if ('' == $viewFile) {
            /**
             * @var \System\Router $router
             */
            $router             = $this->kernel->router;
            $controllerViewPath = $this->uncamelize(preg_replace('/Controller$/', '', $router->getController()));
            $actionViewPath     = $this->uncamelize(preg_replace('/Action$/', '', $router->getAction()));
            $viewFile           = $controllerViewPath . DS . $actionViewPath . '.phtml';
        }

        //include APP_PATH_VIEW . $this->layout;

        if ($this->isValidView($viewFile)) {
            include(APP_PATH_VIEW . $viewFile);
        }

        return ob_get_clean();
    }

    /**
     * @param $viewFile
     *
     * @return bool
     * @throws \Exception
     */
    public function isValidView($viewFile)
    {
        if (!file_exists(APP_PATH_VIEW . $viewFile)) {
            throw new \Exception('View file ' . $viewFile . ' don\'t found');
        }

        return true;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}