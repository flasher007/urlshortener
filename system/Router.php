<?php

namespace System;

class Router
{
    use Helper\StringTrait;

    protected $rules = [];

    /**
     * @var string
     */
    protected $controller = '';
    /**
     * @var string
     */
    protected $action = '';
    /**
     * @var array
     */
    protected $parameters = [];

    public function __construct($config)
    {
        if (isset($config['router'])) {
            foreach ($config['router'] as $mask => $rule) {
                $this->rules[$mask] = $rule;
            }
        }
    }

    /**
     * @param $url
     *
     * @return Router
     */
    public function parseRequestUrl($url)
    {
        // remove leading and trailing slashes
        $url  = trim($url, '/');
        $rule = $this->getMatchedRule($url);
        if (!is_array($rule) || (is_array($rule) && count($rule) < 2)) {
            //Kernel::getInstance()->response->sendNotFoundAndExit();
        }
        $rule[] = [];
        list($controller, $action, $parameters) = $rule;
        $controller = $this->camelize($controller, true) . 'Controller';
        $action     = $this->camelize($action, false) . 'Action';
        foreach ($parameters as $key => $value) {
            Kernel::getInstance()->request->addQueryVar($key, $value);
        }

        $this->setParsedUrl($controller, $action, $parameters);

        return $this;
    }

    /**
     * @param $url
     *
     * @return bool|mixed
     */
    protected function getMatchedRule($url)
    {
        foreach ($this->rules as $mask => $rule) {
            if ('' == $mask) {
                if ($mask == $url) {
                    return $rule;
                }
            } else {
                if (preg_match($mask, $url, $matches)) {
                    if (isset($rule[2])) {
                        foreach ($rule[2] as $key => &$value) {
                            if (':' == $value{0}) {
                                $matchGroup = substr($value, 1);
                                $value      = isset($matches[$matchGroup]) ? $matches[$matchGroup] : '';
                            }
                        }
                    }

                    return $rule;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    protected function setParsedUrl($controller = '', $action = '', $parameters = '')
    {
        $this->setController($controller);
        $this->setAction($action);
        $this->setParameters($parameters);
    }

    public function createUrl($controller, $action, $parameters = [])
    {
        /**
         * @todo url builder by router rules
         */
        return Kernel::getInstance()->request->getBaseUrl() . '/' . $parameters['url'];
    }

}