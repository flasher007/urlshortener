# Test application "URL shortener" #

## Demo ##
You can try demo at http://url.aavolkov.ru/

## Installation ##

*  Download web application into prepared directory for example /var/www/url.sh
   
```
#!

git clone git@bitbucket.org:flasher007/urlshortener.git
```

* Run command in console
```
#!

php composer.phar install
```

* Create new mysql database
for example create database named 'urlshortener'

* load database dump from /var/www/url.sh/dump.sql

## Configuring ##

* Change config options in /var/www/url.sh/config/application.php