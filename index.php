<?php

define('ROOT_PATH', __DIR__);
define('DS', DIRECTORY_SEPARATOR);
define('APP_PATH', ROOT_PATH . DS . 'src' . DS);
define('APP_PATH_VIEW', ROOT_PATH . DS . 'src' . DS . 'views' . DS);

include 'vendor/autoload.php';
use System\Kernel;

$config = include 'config/application.php';
$kernel = Kernel::getInstance($config);
$kernel->run();