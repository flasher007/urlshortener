<?php

class UrlShortenerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var \System\DB\PDOConnection
     */
    protected $db = null;

    protected function setUp()
    {
        $config   = include dirname(__DIR__) . '/config/application.php';
        $this->db = \System\DB\PDOConnection::getConnection($config);
    }

    /**
     * @todo add some assert to this test
     */
    public function testUrlAdd()
    {
        $urlModel = new \App\Model\Url();

        $url     = 'http://test.url.local/';
        $urlData = [
            'url'      => $url,
            'shorturl' => \App\Service\UrlShortener::encode($url)
        ];

        $urlModel->populate($urlData);
        $urlModel->save();

        return $urlModel;
    }

    /**
     * @depends testUrlAdd
     */
    public function testCheckUrl($url)
    {
        $urlModel = new \App\Model\Url();
        $existUrl = $urlModel->findOneBy('url', 'http://test.url.local/');
        $this->assertInstanceOf(get_class($urlModel), $existUrl);
        $this->assertEquals($existUrl->url, $url->url);
        $this->assertEquals($existUrl->shorturl, $url->shorturl);
        $this->assertEquals($existUrl->id, $url->id);
    }

    /**
     *
     */
    public function tearDown()
    {
        $this->db = null;
    }


}